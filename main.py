# Multiprocesamiento Main
from deteccion_video import deteccion_video
import sys
import time
import concurrent.futures

if __name__ == "__main__":
    listaURLVideos = []
    for i in sys.argv:
        listaURLVideos.append(str(i))
    listaURLVideos.pop(0)
    '''
    El método ThreadPoolExecutor trabaja de manera similar a ProcessPoolExecutor, pero se trata de mecanismos distintos:
    ProcessPoolExecutor ejecuta cada worker como un proceso hijo separado   
    ThreadPoolExecutor ejecuta cada uno de los workers como un subproceso (thread) dentro del proceso principal
    '''
    # El método "ProcessPoolExecutor" permite 
    # ejecutar varias instancias (workers) 

    print("-----------------Datos de ejecucion-----------------" + '\n')
    start = time.perf_counter()
    with concurrent.futures.ProcessPoolExecutor() as executor:    
            executor.submit(deteccion_video,listaURLVideos[0],1)
            executor.submit(deteccion_video,listaURLVideos[1],0)
            #executor.submit(deteccion_video(i,1))
    print("----------------------------------------------------")
    
