from __future__ import division
from models import *
from utils.utils import *
from utils.datasets import *
from PIL import Image
from torch.autograd import Variable
from pathlib import Path
import os
import sys
import argparse
import cv2
import torch
import time

# Libreria para graficar
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


def Convertir_RGB(img):
    # Convertir Blue, green, red a Red, green, blue
    b = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    r = img[:, :, 2].copy()
    img[:, :, 0] = r
    img[:, :, 1] = g
    img[:, :, 2] = b
    return img


def Convertir_BGR(img):
    # Convertir red, blue, green a Blue, green, red
    r = img[:, :, 0].copy()
    g = img[:, :, 1].copy()
    b = img[:, :, 2].copy()
    img[:, :, 0] = b
    img[:, :, 1] = g
    img[:, :, 2] = r
    return img


def Generar_grafico(diccionario):
    # Generar un grafico
    fig, ax = plt.subplots()
    # Etiqueta en el eje Y
    ax.set_ylabel('Clases')
    # Etiqueta en el eje X
    ax.set_title('Cantidad de detecciones por clase')
    for x in diccionario:
        plt.bar(x, diccionario[x])
    plt.savefig('Grafico.png')


def deteccion_video(url_video, tipoEjecucion):
    try:
        # Variables locales
        localDir = ""
        start = time.perf_counter()
        clasesDic = {}
        fileName = str(url_video)
        auxFrame = 0

        image_folder = "data/samples"
        model_def = "config/yolov3-custom.cfg"
        weights_path = "checkpoints/yolov3_ckpt_99.pth"
        class_path = "data/custom/classes.names"
        conf_thres = 0.8
        nms_thres = 0.4
        batch_size = 2
        n_cpu = 0
        img_size = 416
        directorio_video = fileName
        checkpoint_model = "checkpoints/yolov3_ckpt_99.pth"

        #Utiliza el modelo predefinido de COCO
        if tipoEjecucion == 1:
            image_folder = "data/samples"
            model_def = "config/yolov3.cfg"
            weights_path = "weights/yolov3.weights"
            class_path = "data/coco.names"
            conf_thres = 0.8
            nms_thres = 0.4
            batch_size = 2
            n_cpu = 0
            img_size = 416
            directorio_video = fileName
            checkpoint_model = ""

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = Darknet(model_def, img_size=img_size).to(device)

        if weights_path.endswith(".weights"):
            model.load_darknet_weights(weights_path)
        else:
            model.load_state_dict(torch.load(weights_path))

        model.eval()
        classes = load_classes(class_path)
        Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

        # Obtiene el video a analizar.
        cap = cv2.VideoCapture(directorio_video)
        colors = np.random.randint(
            0, 255, size=(len(classes), 3), dtype="uint8")

        # Carpeta donde se guardan los resultados
        os.chdir('outputs')  # accede al directorio
        os.mkdir(str(fileName))  # crea una nueva carpeta
        os.chdir(str(fileName))  # accede al directorio
        localDir = str(os.getcwd())

        # Archivo con la informacion
        file = open('Resultados.txt', "w")
        file.write("------------Resultados de la detección------------" + '\n'
                   + "Nombre del Video: " + '\n'
                   + "Objetos:" + '\n')

        # Crea el archivo mp4
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter('output.avi',  fourcc, 5, (1280, 960))

        while cap:
            ret, frame = cap.read()
            # Mostrar Segundos del frame (cap.get(cv2.CAP_PROP_POS_MSEC)/1000)
            if ret is False:
                break
            frame = cv2.resize(frame, (1280, 960),
                               interpolation=cv2.INTER_CUBIC)
            # LA imagen viene en Blue, Green, Red y la convertimos a RGB que es la entrada que requiere el modelo
            RGBimg = Convertir_RGB(frame)
            imgTensor = transforms.ToTensor()(RGBimg)
            imgTensor, _ = pad_to_square(imgTensor, 0)
            imgTensor = resize(imgTensor, 416)
            imgTensor = imgTensor.unsqueeze(0)
            imgTensor = Variable(imgTensor.type(Tensor))

            with torch.no_grad():
                detections = model(imgTensor)
                detections = non_max_suppression(
                    detections, conf_thres, nms_thres)

            for detection in detections:
                # Cada 20 frames guarda una imagen
                if auxFrame == 20:
                    auxFrame = 0

                if detection is not None:
                    detection = rescale_boxes(
                        detection, img_size, RGBimg.shape[:2])
                    for x1, y1, x2, y2, conf, cls_conf, cls_pred in detection:
                        box_w = x2 - x1
                        box_h = y2 - y1
                        color = [int(c) for c in colors[int(cls_pred)]]
                        data = "       Se detectó {} en el segundo: {}".format(
                            classes[int(cls_pred)], cap.get(cv2.CAP_PROP_POS_MSEC)/1000)
                        frame = cv2.rectangle(
                            frame, (x1, y1 + box_h), (x2, y1), color, 5)

                        # Nombre de la clase detectada
                        cv2.putText(
                            frame, classes[int(cls_pred)], (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 1, color, 5)
                        # Certeza de prediccion de la clase
                        cv2.putText(frame, str("%.2f" % float(
                            conf)), (x2, y2 - box_h), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 5)

                    if float(conf) >= 0.90 and auxFrame == 0:
                        # Diccionario que lleva el conteo del numero de clases detectadas
                        if classes[int(cls_pred)] in clasesDic:
                            # accede al directorio
                            os.chdir(str(classes[int(cls_pred)]))
                            clasesDic[classes[int(cls_pred)]] += 1
                        else:
                            clasesDic[classes[int(cls_pred)]] = 1

                            # crea una nueva carpeta con el nombre de la clase
                            os.mkdir(str(classes[int(cls_pred)]))
                            # accede al directorio
                            os.chdir(str(classes[int(cls_pred)]))

                        # Escribe los datos
                        file.write(str(data)+'\n')
                        cv2.imwrite(
                            str(cap.get(cv2.CAP_PROP_POS_MSEC)/1000)+'.jpg', Convertir_RGB(frame))

                        os.chdir(localDir)  # accede al directorio
                        # Agrega la imagen en un video
                        out.write(frame)

                    auxFrame += 1

        # Escribir en el archivo 'Resultados' la cantidad de objetos detectados por clase.
        file.write(
            '\n' + "------------Cantidad de detecciones por clase------------" + '\n')
        for x in clasesDic:
            file.write(str(x) + ": " + str(clasesDic[x]) + '\n')

        # Genera el grafico
        Generar_grafico(clasesDic)

        # Muestra en consola la duracion total
        print("Nombre del archivo: " + str(fileName) + '\n'
              + f'Tiempo: {time.perf_counter() - start}' + " segundos"
              + '\n' + '\n')

        # Se cierran todos los procesos
        file.close()
        cap.release()
        cv2.destroyAllWindows()
    except Exception as err:
        print("Ha ocurrido un error de tipo: ", type(err))
