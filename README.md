# Guía de instalación del modelo de detección de objetos en video
Este repo está basado en el proyecto [PyTorch YOLOv3](https://github.com/eriklindernoren/PyTorch-YOLOv3) para correr detección de objetos sobre video.

El objetivo de este proyecto es el entrenamiento y uso del multiproceso para la detección de objetos específicos en un video o película con la red neuronal YOLO.

Por default este modelo esta pre entrenado para detectar pistolas (armas), y cuenta con la opción de utilizar el data set de COCO, el cual incluye 80 clases para ser detectadas.

# Requerimientos previos

Para esta guía de instalación se requiere tener previamente instalado python 3.6 o superior, Anaconda3 y utilizar la consola de Anaconda (o la del sistema en caso de haber configurado la dirección de Anaconda en el PATH).

# Crear ambiente
Para tener en orden nuestras paqueterías de python primero vamos a crear un ambiente llamado "training" el cual tiene la versión 3.6 de python.
``` 
conda create -n training python=3.6
```

Activamos el ambiente training para asegurarnos que estemos en el ambiente correcto al momento de hacer la instalación de todas las paqueterías necesarias.
```
activate training
```

#Instalación componentes
Antes de instalar los requerimientos en el ambiente activado se necesita instalar pytorch, torchvision y cudatoolkit.
```
conda install pytorch==1.5.0 torchvision==0.6.0 cudatoolkit=9.2 -c pytorch
```

# Instalación de las paqueterias
Una vez dentro del ambiente y con los componentes extra ya instalados vamos a instalar todas las paqueterías necesarias para correr nuestro detector de objetos en video, la lista de los paqueterías y versiones a instalar están dentro del archivo requirements.txt por lo cual se hace referencia a ese archivo.
```
pip install -r requirements.txt
```

# Descargar los pesos del modelo entrenado 
Para poder correr el modelo de yolo tendremos que descargar los pesos de la red neuronal ya sea ejecutando el bash o directamente desde https://pjreddie.com/media/files/yolov3.weights , los pesos son los valores que tienen todas las conexiones entre las neuronas de la red neuronal de YOLO, este tipo de modelos son muy pesados de entrenar desde cero por lo cual descargar el modelo pre entrenado es una buena opción.

```
bash weights/download_weights.sh
```

Se mueven los pesos descargados a la carpeta llamada weights.
```
mv yolov3.weights weights/
```
# Correr el detector de objetos en video 
Por último corremos este comando el cual permite hacer detección de objetos sobre varios videos almacenado dentro de una lista del sistema.
```
python main.py <ejemplo1.mp4> <ejemplo2.mp4>
```

# Guía entrenamiento 

Para entrenar un modelo con las clases que se quieran y no utilizar las que vienen por default, se puede entrenar el modelo. Estos son los pasos que se tienen que seguir:

Primero se debe contar con un dataset, en donde ya se encuentren las imágenes etiquetadas con el formato Yolo.

En el archivo ```confing/custom.data```, se tiene que modificar la primera linea con el número de clases a entrenar
```
classes= <Numero de clases>
train=data/custom/train.txt
valid=data/custom/valid.txt
names=data/custom/classes.names
```

Desde la carpeta config correremos el archivo create_custom_model para generar un archivo .cfg el cual contiene información sobre la red neuronal para correr las detecciones.
```
cd config
bash create_custom_model.sh <Numero_de_clases_a_detectar>
cd ..
```
Descargamos la estructura de pesos de YOLO corriendo el archivo ```dowload_darknet.sh``` o se puede descargar directamente desde el enlace: https://pjreddie.com/media/files/darknet53.conv.74 y guardar el archivo en la carpeta weights.
```
cd weights
bash download_darknet.sh
cd ..
```

# Agregar las imagenes al modelo
Las imágenes etiquetadas tienen que estar en el directorio data/custom/images y los labels de las imágenes tienen que estar en data/custom/labels. Es importante que tanto el nombre del archivo de la imagen sea igual al txt, ejemplo: (Imagen)imagen.jpg / (Label)imagen.txt
El archivo ```data/custom/classes.names``` debe almacenar el nombre de las clases a entrenar.

Para validar que la ruta de cada imagen y label se tienen que generar dos archivos ```data/custom/valid.txt``` y ```data/custom/train.txt```, para esto hay que ejecutar el siguiente scrip desde el repositorio:
```
python split_train_val.py
```

## Entrenar
Se escriben los siguientes comandos:
 ```
python train.py --model_def config/yolov3-custom.cfg --data_config config/custom.data --pretrained_weights weights/darknet53.conv.74 --batch_size 2
 ```

## Correr detección de objetos en video con nuestras clases
En el archivo main.py se tienen los executors, los cuales se encargan de ejecutar la función deteccion_video, en donde se pasan como parámetros los argumentos recibidos de la consola, que en este caso serían los archivos .mp4, y como segundo parámetro un valor numérico en cargado de indicar con qué modelo se quiere realizar la detención, ya sea 1 para utilizar el modelo predefinido de COCO, o 0 para el modelo customizado.


```
with concurrent.futures.ProcessPoolExecutor() as executor:    
            executor.submit(deteccion_video,listaURLVideos[0],1)
            executor.submit(deteccion_video,listaURLVideos[1],0)
```
Para correr la detección se ejecuta el siguiente comando:
```
python main.py <ejemplo1.mp4> <ejemplo2.mp4>
```
## Resultados
Los resultados obtenidos se encontrarán en la carpeta ```outputs```, en donde se encontrará un gráfico que muestra la cantidad de objetos detectados, además de un archivo .txt, donde detalla el tiempo del video en el que se hizo una detección, además de mostrar la cantidad de detecciones clasificadas por sus respectivas clases. También en esta misma carpeta se encuentran todas las salidas en formato .jpg de los objetos detectados, cada una guardada en su directorio respectivo.
